﻿using System;

namespace Otus.Teaching.Pcf.Administration.Integration
{
    public class NotifyAdminAboutPartnerManagerPromoCodeMsg
    {
        public Guid? PartnerManagerId { get; set; }
    }

}
