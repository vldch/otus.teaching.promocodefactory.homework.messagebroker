﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Integration
{
    public interface IIntegrationEvents
    {
        Task UpdatePromoCodesEvent(string notificationMessage);
    }
}
