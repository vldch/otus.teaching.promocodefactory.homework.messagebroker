﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Integration;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{

    public class IntegrationEvents : IIntegrationEvents
    {
        private readonly IServiceScopeFactory _scopeFactory;
        public IntegrationEvents(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task GivePromoCodeToCustomer(string notificationMessage)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var _customersRepositoty = scope.ServiceProvider.GetRequiredService<IRepository<Customer>>();
                var _preferencesRepository = scope.ServiceProvider.GetRequiredService<IRepository<Preference>>();
                var _promoCodesRepository = scope.ServiceProvider.GetRequiredService<IRepository<PromoCode>>(); 
                var promocodeNotified = JsonSerializer.Deserialize<GivePromoCodeToCustomerMsg>(notificationMessage);

                try
                {
                    //Получаем предпочтение по имени
                    var preference = await _preferencesRepository.GetByIdAsync(promocodeNotified.PreferenceId);

                    if (preference == null)
                        return;

                    //  Получаем клиентов с этим предпочтением:
                    var customers = await _customersRepositoty
                        .GetWhere(d => d.Preferences.Any(x =>
                            x.Preference.Id == preference.Id));

                    var promocode = new PromoCode();
                    promocode.Id = promocodeNotified.PromoCodeId;
                    promocode.PartnerId = promocodeNotified.PartnerId;
                    promocode.Code = promocodeNotified.PromoCode;
                    promocode.ServiceInfo = promocodeNotified.ServiceInfo;
                    promocode.BeginDate = DateTime.Parse(promocodeNotified.BeginDate);
                    promocode.EndDate = DateTime.Parse(promocodeNotified.EndDate);
                    promocode.Preference = preference;
                    promocode.PreferenceId = preference.Id;

                    promocode.Customers = new List<PromoCodeCustomer>();

                    foreach (var item in customers)
                    {
                        promocode.Customers.Add(new PromoCodeCustomer()
                        {

                            CustomerId = item.Id,
                            Customer = item,
                            PromoCodeId = promocode.Id,
                            PromoCode = promocode
                        });
                    };

                    await _promoCodesRepository.AddAsync(promocode);

                }
                catch (Exception e)
                {
                    Console.WriteLine($"--> Could not update Promocodes for Customers: {e.Message}");
                    throw;
                }
            }
        }
    }
}
